package com.example.objectwithintentsexample;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class YellowFragment extends Fragment {


    // 1. Default changes
    View view;


    // 2. Make button variable
    Button yellowButton;

    public YellowFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // 2. Default change
        this.view =  inflater.inflate(R.layout.fragment_yellow, container, false);


        // 3. add button click handler
        yellowButton = (Button) view.findViewById(R.id.buttonYellow);
        yellowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast m = Toast.makeText(getContext(), "Yellow Button Pressed", Toast.LENGTH_SHORT);
                m.show();
            }
        });



        return this.view;
    }

}
