package com.example.objectwithintentsexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    final String TAG="FRAGMENT_EXAMPLE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void mainButtonPressed(View view) {
        Log.d(TAG, "Main Button pressed");
        Toast m = Toast.makeText(getApplicationContext(), "Main Button Pressed", Toast.LENGTH_SHORT);
        m.show();

        // dynamically load the fragment

        // 1. get a fragment manager & start a new transaction
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        // 2. Tell the manager what fragment you want to load
        ft.replace(R.id.framelayout_main, new PinkFragment());

        // 3. save changes
        ft.commit();


    }
}
